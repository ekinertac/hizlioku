#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os

user = 'ekinertac'
workers = 2
bind = "0.0.0.0:8000"
pidfile = os.path.join(os.getcwd(), 'gunicorn.pid')
