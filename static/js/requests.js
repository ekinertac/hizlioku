// NProgress.start();
// NProgress.done();

$.ajaxSetup({
    beforeSend: function(xhr, settings) {
        xhr.setRequestHeader("X-CSRFToken", csrf);
        NProgress.start();
    },
    complete: function () {
        NProgress.done();
    }
});

isPage('speed_reading_tests', function () {
    // Test answers
    $('.answers a').click(function (event) {
        var url = $(this).attr('data-href');
        var el = $(this);
        $.ajax({
            url: url,
            success: function (response) {
                el.addClass('active');
                $('#next').addClass('btn-success');
            }
        });
    });
});

