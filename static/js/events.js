var count_time = 1200;

isPage('tachistoscope', function() {
    var Tacho = {
        current_word : '',
        status : "startable",

        show_word : function () {
            if(! _.isEmpty(word_list)){
                if(Tacho.status === 'startable'){
                    Tacho.status = 'started';

                    var speed = docCookies.getItem('speed') || 1;

                    window.word_list = _(word_list).shuffle();
                    Tacho.current_word = word_list.shift();
                    var current_word = Tacho.current_word;

                    var gameArea = $('#gameArea');
                    gameArea.append('<span class="word">' + current_word + '</span>');

                    // Sound.metronome();
                    var word = gameArea.find('.word');
                    word.show();

                    var width = word.width() / 2;
                    width = '-' + width + 'px';

                    word.css({
                        'margin-left' : width
                    });

                    setTimeout(function(){
                        word.hide()
                    }, count_time / speed);

                    var input_word = $('#input_word');
                    input_word.focus();
                    input_word.parent().removeClass('success');
                    input_word.parent().removeClass('error');

                }else{
                    Notify.error('Lutfen Kelimeyi Girinizzzaaa');
                }
            }else{
                Notify.error('Kelimeler Tukendi!');
            }
        },
        check_word: function () {
            var input = $('#input_word');
            var word = input.val();
            if(word){
                if(word == Tacho.current_word){
                    Tacho.result(true);
                }else{
                    Tacho.result(false);
                }
            }else{
                Notify.error('Lutfen Kelimeyi Giriniz');
            }
        },
        result: function (status) {
            var input = $('#input_word');
            if(status){
                Point.positive();
                input.parent().addClass('success');
                Notify.success('"'+Tacho.current_word + '" Doğru!');
            }else{
                Point.negative();
                input.parent().addClass('error');
                Notify.error('"'+Tacho.current_word + '" Yanlis!');
            }

            $('#gameArea .word').remove();
            $('#input_word').val('');
            Tacho.status = "startable";

            setTimeout(function (){
                Tacho.show_word();
            }, 1000);

        }
    };


    $('#check_word').click(function () {
        Tacho.check_word();
    });


    var enter_key = function (selector, callback) {
        $(selector).keydown(function(e) {
            if(e.which == 13) {
                callback();
            }
        });
    }

    enter_key('#input_word', function () {
        Tacho.check_word();
    });

    $('#speed .tick').html(docCookies.getItem('speed') || 1);

    $('#speed .dropdown-menu a').click(function () {
        Tacho.status = 'startable';
        $('#gameArea .word').remove();
        $('#input_word').val('');
    })

    $('.start-practice').click(function (event) {
        Tacho.show_word();
    });

    homework_counter();
});

isPage('env_focus', function() {
    var EnvFocus = {
        current_word : '',
        current_postitions : '',
        status : "startable",
        get_random: function () {
            var first = {
                'top' : _.random(0, 95) + '%'
            };
            var second = {};
            var value = _.random(0, 95);
            if (value > 50){
                second = {
                    'right' : (100 - value) + '%'
                }
            }else{
                second = {
                    'left' : value + '%'
                }
            }

            var result = [first, second];
            return result
        },
        show_word : function () {
            if(! _.isEmpty(word_list)){

                if(EnvFocus.status === 'startable'){
                    EnvFocus.status = 'started';

                    var speed = docCookies.getItem('speed') || 1;


                    word_list = _(word_list).shuffle();

                    EnvFocus.current_word = word_list.shift();

                    var gameArea = $('#gameArea');
                    gameArea.append('<span class="word">' + EnvFocus.current_word + '</span>');

                    Sound.metronome();

                    var word = gameArea.find('.word');
                    var pos = EnvFocus.get_random();
                    word.show();
                    word.css(pos[0]).css(pos[1]);

                    setTimeout(function(){
                        word.hide()
                    }, count_time / speed);

                    var input = $('#input_word');
                    input.focus();
                    input.parent().removeClass('success error');

                }else{
                    Notify.error('Lutfen Kelimeyi Giriniz');
                    $('#input_word').focus();
                }
            }else{
                Notify.error('Kelimeler Tukenmistir');
            }

        },
        check_word: function () {
            var input = $('#input_word');
            var word = input.val();
            if(word){
                if(word == EnvFocus.current_word){
                    EnvFocus.result(true);
                }else{
                    EnvFocus.result(false);
                }
            }else{
                Notify.error('Lutfen Kelimeyi Giriniz');
            }
        },
        result: function (status) {
            var input = $('#input_word');
            if(status){
                Point.positive();
                input.parent().addClass('success');
                Notify.success('"'+EnvFocus.current_word + '" Doğru!');
            }else{
                Point.negative();
                input.parent().addClass('error');
                Notify.error('"'+EnvFocus.current_word + '" Yanlis!');
            }

            $('#gameArea').find('.word').remove();
            input.val('');
            EnvFocus.status = "startable";

            setTimeout(function () {
                EnvFocus.show_word();
            }, 1000);

        }
    };


    $('#check_word').click(function () {
        EnvFocus.check_word();
    });


    var enter_key = function (selector, callback) {
        $(selector).keydown(function(e) {
            if(e.which == 13) {
                callback();
            }
        });
    }

    enter_key('#input_word', function () {
        EnvFocus.check_word();
    });

    $('#speed .tick').html(url('#speed') || 1);

    $('#speed .dropdown-menu a').click(function () {
        EnvFocus.status = 'startable';
        $('#gameArea .word').remove();
        $('#input_word').val('');
    });

    $('.start-practice').click(function (event) {
        EnvFocus.show_word();
    });

    homework_counter();
});

isPage('multi_focus', function() {
    var MultiFocus = {
        status : "startable",
        word_status : '',
        show_word : function () {
            if(! _.isEmpty(word_list)){

                MultiFocus.status = "started";

                var speed = docCookies.getItem('speed') || 1;

                word_list = _(word_list).shuffle();

                var shifted = word_list.shift();
                var sliced = shifted.split('|');

                var left_word = _.strip(sliced[0]);
                var right_word = _.strip(sliced[1]);

                if(left_word == right_word){
                    MultiFocus.word_status = true;
                }else{
                    MultiFocus.word_status = false;
                }

                MultiFocus.place_words(left_word, right_word);

                Sound.metronome();
                $('#gameArea .words').show();

                setTimeout(function(){
                    MultiFocus.delete_words();
                    $('#gameArea .words').hide();
                }, count_time / speed);

            }else{
                Notify.error('Kelimeler Tukenmistir', 'top');
            }
        },
        place_words: function (left, right) {
            $('.words .left').html(left);
            $('.words .right').html(right);
        },
        delete_words: function () {
            $('.words .left').empty();
            $('.words .right').empty();
        },
        result : function (status) {
            if(MultiFocus.status === 'started'){
                if(status === MultiFocus.word_status){
                    Point.positive();
                    Notify.success('Bildiniz!', 'top');
                    setTimeout(function () {
                        MultiFocus.show_word();
                    }, 1000);

                }else{
                    Point.negative();
                    Notify.error('Bilemediniz!', 'top');
                    MultiFocus.word_status = '';
                    setTimeout(function () {
                        MultiFocus.show_word();
                    }, 1000);
                }
            }else{
                Notify.error('Önce alıştırmayı başlatmanız gerekmektedir.', 'top');
            }
        }
    };

    $('#gameArea .words').hide();

    $('#speed .tick').html(docCookies.getItem('speed') || 1);

    $('.start-practice').click(function (event) {
        MultiFocus.show_word();
    });

    // Result Clicks
    $('.true-choice').click(function () { MultiFocus.result(true); });
    $('.false-choice').click(function () { MultiFocus.result(false); });

    homework_counter();
});

isPage('shadowing', function() {
    var Shadowing = {
        word_list : null,
        startable: true,
        total_words : null,
        init: function () {
            Shadowing.get_article();
        },
        get_article: function () {
            var word_list = $('#word_list').text();
            word_list = _.words(_.clean(word_list));
            Shadowing.word_list = word_list;
            _(Shadowing.word_list).each( function(value) {
                $('.text-reading .context').append('<span class="open">' + value + ' </span>');
            });

            Shadowing.total_words = word_list.length;
        },
        start: function () {
            if(Shadowing.startable){

                reading_counter($('html').attr('id'));

                Shadowing.startable = false;
                var speed = docCookies.getItem('speed') || 1;
                var level = docCookies.getItem('level') || 1;

                window.word_interval = setInterval(function () {
                    if(! _.isEmpty(Shadowing.word_list)){

                        Sound.metronome();

                        _(level).times(function(n){
                            $('.text-reading .context .open:first')
                                .addClass('shadow')
                                .removeClass('open');

                            Shadowing.word_list.shift();

                            percentage(Shadowing);

                        });
                    }else{
                        clearInterval(word_interval);
                        Shadowing.startable = true;
                    }
                }, count_time / speed);

                TextSpeed(speed);

                homework_counter();

            }else{
                Notify.error('Tekrar baslatamazsiniz')
            }
        },
        pause: function () {
            Shadowing.startable = true;
            clearInterval(word_interval);
        }
    }

    $('#speed .tick').html(docCookies.getItem('speed') || 1);
    $('#level .tick').html(docCookies.getItem('level') || 1);

    Shadowing.init();

    // console.log(Shadowing.total_words);

    $('.start-practice').click(function () {
        Shadowing.start();
    });

    $('.pause-practice').click(function () {
        Shadowing.pause();
    });
});

isPage('grouping', function() {
    var Grouping = {
        word_list : null,
        startable: true,
        total_words : null,
        init: function () {
            Grouping.get_article();
        },
        get_article: function () {

            var word_list = $('#word_list').text();
            word_list = _.words(_.clean(word_list));
            Grouping.word_list = word_list;
            _(Grouping.word_list).each( function(value) {
                $('.text-reading .context').append('<span class="block">' + value + ' </span>');
            });

            Grouping.total_words = word_list.length;
        },
        start: function () {
            if(Grouping.startable){

                reading_counter($('html').attr('id'));

                Grouping.startable = false;
                var speed = docCookies.getItem('speed') || 1;
                var level = docCookies.getItem('level') || 1;

                window.word_interval = setInterval(function () {
                    if(! _.isEmpty(Grouping.word_list)){
                        Sound.metronome();

                        _(level).times(function () {

                            $('.text-reading .context .block:first').removeClass('block');
                            Grouping.word_list.shift();
                            percentage(Grouping);

                        });

                    }else{
                        clearInterval(word_interval);
                        Grouping.startable = true;
                    }
                }, count_time / speed);

                TextSpeed(speed);

                homework_counter();

            }else{
                Notify.error('Tekrar baslatamazsiniz')
            }
        },
        pause: function () {
            Grouping.startable = true;
            clearInterval(word_interval);
        }
    }

    $('#speed .tick').html(docCookies.getItem('speed') || 1);
    $('#level .tick').html(docCookies.getItem('level') || 1);

    Grouping.init();

    $('.start-practice').click(function () {
        Grouping.start();
    });

    $('.pause-practice').click(function () {
        Grouping.pause();
    });
});

isPage('block_reading', function() {
    var BlockReading = {
        word_list : null,
        startable: true,
        total_words : null,
        init: function () {
            BlockReading.get_article();
        },
        get_article: function () {

            var word_list = $('#word_list').text();
            word_list = _.words(_.clean(word_list));
            BlockReading.word_list = word_list;
            BlockReading.total_words = word_list.length;

        },
        start: function () {
            if(BlockReading.startable){

                reading_counter($('html').attr('id'));

                BlockReading.startable = false;
                var speed = docCookies.getItem('speed') || 1;
                var level = docCookies.getItem('level') || 1;

                if(! _.isEmpty(BlockReading.word_list)){

                    window.interval = setInterval(function () {
                        if (!_.isEmpty(BlockReading.word_list)) {
                            $('#gameArea .blocks').empty();
                            _(level).times(function () {
                                $('#gameArea .blocks').append((BlockReading.word_list.shift() || '') + ' ');
                                percentage(BlockReading);
                            });

                            Sound.metronome();
                        }
                    }, count_time / speed);

                    TextSpeed(speed);

                }else{
                    clearInterval(window.interval);
                    BlockReading.startable = true;
                }

                homework_counter();

            }else{
                Notify.error('Uygulama suan başlamış durumda, tekrar başlatamazsınız.');
            }
        },
        pause: function () {
            clearInterval(window.interval);
            BlockReading.startable = true;
        }
    }

    $('#speed .tick').html(docCookies.getItem('speed') || 1);
    $('#level .tick').html(docCookies.getItem('level') || 1);

    BlockReading.init();

    $('.start-practice').click(function () {
        BlockReading.start();
    });

    $('.pause-practice').click(function () {
        BlockReading.pause();
    });
});

isPage('speed_reading_tests_detail', function() {

    var overflow = $('.overflow'),
        start_time = 0,
        word_count = _.words($('.text-reading').text()).length,
        interval = null,
        status = 'not_started';

    overflow.hover(function () {
        $('.text-reading').css('overflow', 'hidden');
    }, function () {
        $('.text-reading').css('overflow', 'auto');
    });

    $('.start-practice').click(function() {
        if(status == 'not_started' || status == 'paused'){
            overflow.hide();

            interval = setInterval(function () {
                start_time += 10;
                console.log(start_time);
            }, 10);

            $('#buttons .btn').removeClass('disabled');
            $(this).addClass('disabled');

            status = 'started';

        }
    });

    $('.pause-practice').click(function () {
        if(status == 'started'){
            console.log(start_time);
            clearInterval(interval);
            overflow.show();
            status = 'paused';

            $('#buttons .btn').removeClass('disabled');
            $(this).addClass('disabled');

        }
    });

    $('.finish-practice').click(function (event) {
        event.preventDefault();
        var url = $(this).attr('href');

        if ((status == 'started' || status == 'paused') && start_time > 3000) {
            $.ajax({
                url: '/api/test_read_finish/' + start_time + '/' + word_count + '/',
                success: function(response){
                    window.location.href = url;
                }
            });
        }

        if(start_time < 30000){
            Notify.error('30 Saniyeden once bitiremezsiniz','top' ,3000);
        }

    });
});

isPage('speed_reading_result', function() {
    $.getJSON('/api/test_graph/', function (response) {
        // $('.context').highcharts('StockChart', {
        //     title: {
        //         text: 'Hızlı Okuma Test Raporları'
        //     },
        //     yAxis: [
        //         { // Primary yAxis
        //             title: {
        //                 text: 'Okuma Hızı',
        //                 style: {
        //                     color: '#4572A7'
        //                 }
        //             },
        //             opposite: true,
        //             labels: {
        //                 formatter: function () {
        //                     return this.value + ' k/d';
        //                 }
        //             }

        //         },
        //         { // Secondary yAxis
        //             gridLineWidth: 0,
        //             title: {
        //                 text: 'Doğru Cevap Yüzdesi',
        //                 style: {
        //                     color: '#AA4643'
        //                 }
        //             },
        //             labels: {
        //                 formatter: function () {
        //                     return this.value + '% ';
        //                 }
        //             }
        //         }
        //     ],
        //     tooltip: {
        //         shared: true
        //     },

        //     series: [
        //         {
        //             name: 'Okuma Hızı',
        //             color: '#4572A7',
        //             type: 'spline',
        //             yAxis: 0,
        //             marker: {enabled: true},
        //             data: response.speed,
        //             tooltip: {
        //                 valueSuffix: ' kelime/dakika'
        //             }
        //         },
        //         {
        //             name: 'Doğru Cevap Yüzdesi',
        //             color: '#AA4643',
        //             type: 'spline',
        //             yAxis: 1,
        //             marker: {enabled: true},
        //             data: response.question,
        //             tooltip: {
        //                 valuePrefix: '% '
        //             }
        //         }
        //     ]
        // });
    });
});

isPage('eye_focusing', function() {
    var EyeFocus = {
        startable: true,
        positions: null,
        cycle: null,
        init: function () {
            EyeFocus.get_positions();
        },
        get_positions: function () {
            EyeFocus.positions = item_positions;
        },
        start: function () {
            if(EyeFocus.startable){

                homework_counter();

                var tmp = _.template($('#fullscreen_template').html(), {});
                $('body').append(tmp);
                var fs = $('#fullscreen');
                var _resizeFS = function() {
                    fs.css({
                        'height': ($(window).height() - (33 + 8))+ 'px'
                    });
                };

                _resizeFS();
                $(window).resize(function() {
                    _resizeFS();
                });

                $('.close').click(function() {
                    EyeFocus.stop();
                });

                var img = $('#gameArea .item');
                fs.find('.fs-gameArea').append(img);

                EyeFocus.startable = false;
                var speed = docCookies.getItem('speed') || 1;

                var auto_refresh = "";

                if(auto_refresh != ""){
                    return;
                }

                var len = EyeFocus.positions.length;
                var index = 0;
                var start_over = function () {
                    EyeFocus.cycle = setInterval(function () {

                        var position = EyeFocus.positions[index];
                        index++;
                        len--;

                        fs.find('.fs-gameArea .item').css({
                            'top': position[0] + '%',
                            'left': position[1] + '%',
                        });

                        Sound.metronome();

                        if(len == 0){
                            clearInterval(EyeFocus.cycle);
                            len = EyeFocus.positions.length;
                            index = 0;
                            start_over();
                        }

                    }, count_time / speed);
                }

                start_over();

            }else{
                Notify.error('Uygulama suan başlamış durumda, tekrar başlatamazsınız.');
            }
        },
        stop: function () {
            EyeFocus.startable = true;
            clearInterval(EyeFocus.cycle);
            $('#fullscreen').remove();
            $('#gameArea .item').css({
                'top': EyeFocus.positions[0][0],
                'left': EyeFocus.positions[0][1],
            });
        }
    }

    $('#speed .tick').html(docCookies.getItem('speed') || 1);

    EyeFocus.init();

    $('.start-practice').click(function () {
        EyeFocus.start();
    });

    $('.stop-practice').click(function () {
        EyeFocus.stop();
    });

    $('#speed a').click(function () {
        EyeFocus.stop();
    });
});

isPage('performance', function() {
    $('.performance-categories .dropdown-menu a').click(function(event){

        event.preventDefault();
        var url = $(this).attr('data-href');
        var app_name = $(this).html();
        $('#chart-container').empty();

        if(url){
            if($.inArray(url, ['shadowing', 'grouping', 'block_reading']) > -1){
                
                return $.ajax({
                    url: url,
                    success: function(response){
                        $('#chart-container').highcharts('StockChart', {
                            plotOptions: {series: { animation: { duration: 2000}}},
                            rangeSelector: {
                                selected: 1,
                                buttons: [
                                    {type: "month", count: 1, text: "1 Ay"},
                                    {type: "month", count: 3, text: "3 Ay"},
                                    {type: "month", count: 6, text: "6 Ay"},
                                    {type: "week", count: 1,  text: "1 Hafta"},
                                    {type: "year", count: 1, text: "1 Yıl"},
                                    {type: "all", text: "Tümü"}
                                ],
                                buttonTheme: { width: 40 }
                            },
                            title: { text: app_name + ' Raporu' },
                            series: [
                                {
                                    name: 'Toplam Saniye',
                                    data: response.positive
                                }
                            ]
                        });
                    }
                });

            }else{
                return $.ajax({
                    url: url,
                    success: function(response){

                        $('#chart-container').highcharts('StockChart', {
                            plotOptions: {series: { animation: { duration: 2000}}},
                            rangeSelector: {
                                selected: 1,
                                buttons: [
                                    {type: "month", count: 1, text: "1 Ay"},
                                    {type: "month", count: 3, text: "3 Ay"},
                                    {type: "month", count: 6, text: "6 Ay"},
                                    {type: "week", count: 1,  text: "1 Hafta"},
                                    {type: "year", count: 1, text: "1 Yıl"},
                                    {type: "all", text: "Tümü"}
                                ],
                                buttonTheme: { width: 40 }
                            },
                            title: { text: app_name + ' Raporu' },
                            series: [
                                {
                                    name: 'Doğru',
                                    data: response.positive
                                },
                                {
                                    name: 'Yanlış',
                                    data: response.negative
                                }
                            ]
                        });
                    }
                });
            }
        }else{
            window.location.href = $(this).attr('href');
        }

    });
});

isPage('exam_detail', function() {
    var Exam = {
        questions: [],
        time : 0,
        interval: null,
        init: function () {
            var questions = $('.testQuestion');
            $.each(questions, function(){
                Exam.questions.push(this);
            });

            var words = _.words($('#gameArea').text()).length;
            $('input[name="word_count"]').val(words);

        },
        start: function () {

            Exam.interval = setInterval(function() {

                Exam.time += 10;
                $('input[name="ms"]').val(Exam.time);

            }, 10);

            if(! _.isEmpty(Exam.questions)){
                $('.testQuestion').hide();
                var current_question = Exam.questions.shift();
                $(current_question).show();
                $('.start').html('Sonraki Soru');
            }

            if (_.isEmpty(Exam.questions)) {
                var start = $('.start');
                start.html('Testi Bitir').attr('class', 'btn btn-danger start');
            }

            homework_counter();

        }
    };

    Exam.init();

    $('.start').click(function () {
        if(Exam.questions.length != 0){
            Exam.start();
        }else{
            var ms = $('input[name="ms"]');
            $('input[name="total_ms"]').val(ms.val());
            ms.remove();
            $('form').submit();
        }
    });
});

isPage('homework_index', function () {
    var table = $('#homeworks');
    $.ajax({
        url: '/homework_user',
        success: function (response) {
            _.each(response.works, function (item) {
               table.find('tr[data-id="'+item+'"] .status').html('<i class="icon-ok" style="color:#5bb75b"></i>');
            });
            table.show();
        }
    });

    $('.homeworkRedirect').click(function (event) {
        event.preventDefault();

        var type = $(this).attr('data-type') || '';
        var level = $(this).attr('data-level') || '';
        var speed = $(this).attr('data-speed') || '';
        var url = $(this).attr('data-url') || '';
        var minute = $(this).attr('data-minute') || '';
        var id = $(this).attr('data-id') || '';
        var is_reading = parseInt($(this).attr('data-is-reading'), 10) || '';

        if(is_reading){
            docCookies.setItem('speed', speed, '/');
            docCookies.setItem('level', level, '/');
            docCookies.setItem('minute', minute, '/');
            window.location.href = "/practices/" + type + '/?homework=' + id;
        }else{
            var path = "/practices/" + type + '/' + level + '/?homework=' + id;
            docCookies.setItem('speed', speed, '/');
            docCookies.setItem('minute', minute, '/');
            window.location.href = path;
        }
    });
});

isPage('*', function() {
    // Time ticker
    $('.last_login').find('strong').stopwatch({
        startTime: -(new Date($('.last_login').attr('data-date')) - (new Date()))
    }).stopwatch('start');

    $('.sound').click(function(){
        var el = $(this);
        if(el.find('i').hasClass('icon-volume-up')){
            el.find('i').removeClass('icon-volume-up')
                .addClass('icon-volume-off');
            docCookies.setItem('volume', 'off', '/');
        }else{
            el.find('i').removeClass('icon-volume-off')
                .addClass('icon-volume-up');
            docCookies.setItem('volume', 'on', '/');
        }
    });

    if(docCookies.getItem('volume') == 'on'){
        $('.sound i').addClass('icon-volume-up');
    }else{
        $('.sound i').addClass('icon-volume-off');
    }

    if (! docCookies.hasItem('volume')) {
        $('.sound i').addClass('icon-volume-up');
        docCookies.setItem('volume', 'on', '/');
    }

});