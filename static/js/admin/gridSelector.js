if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
            ? args[number] : match;
        });
    };
}

$(document).ready(function (){

    if($('body').hasClass('practices-eyefocusmodel change-form')){
        var tmp = _.template($('#grid_template').html(), {});
        $('body').append(tmp);

        var textarea = $('textarea');

        textarea.after('<p><a class="clear" href="#">Clear</a></p>')

        $('.clear').click(function(e) {
            e.preventDefault();
            textarea.html('');
        });

        $('.grid td').click(function () {
            $('.grid td').removeClass('clicked');
            $(this).addClass('clicked');
            var x = $(this).attr('data-x');
            var y = $(this).attr('data-y');
            var str = '';
            if(! textarea.html() == ''){
                str += ', '
            }
            str += '[{0}, {1}]'.format(y, x)


            textarea.append(str);
        });
    }

});