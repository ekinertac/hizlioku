// Underscore.string extension activation
_.mixin(_.string.exports());

/**
 * Javascript String Formatting
 * Usage:
 * >>> "I'm {0} to make him an offer he can't {1}.".format("going", "refuse")
 * >>> I'm going to make him an offer he can't refuse.
 */
if (!String.prototype.format) {
    String.prototype.format = function() {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function(match, number) {
            return typeof args[number] != 'undefined'
            ? args[number] : match;
        });
    };
}


var Notify = {
    show: function (message, type, position, time) {
        if(position == null){
            position = 'bottom';
        }

        if(time == null){
            time = 1000;
        }

        if(message == null){
            return Debug.parameter('message');
        }

        if (type == null){
            return Debug.parameter('type');
        }

        var template = _.template( $('#notify_message').html(), {
            message: message,
            type: type
        });

        var gameArea = $('#gameArea');
        gameArea.append(template);
        var notify_el = gameArea.find('.notify');

        var width = notify_el.width() / 2;
        notify_el.css({
            'margin-left' : '-' + width + 'px'
        });

        if(position == 'top'){
            notify_el.css({
                'margin-left' : '-' + width + 'px',
                'bottom' : 'auto',
                'top': '20px'
            });
        }

        return Notify.hide(time);

    },
    hide: function (time) {
        var notify_el = $(".notify");
        return setTimeout((function() {
            return notify_el.fadeOut(500, function () {
                return notify_el.remove();
            });
        }), time);
    },
    error : function (message, position, time) {
        return Notify.show(message, 'error', position, time);
    },
    success: function (message, position, time) {
        return Notify.show(message, 'success', position, time);
    }
};

var Debug = {
    parameter : function (param) {
        return alert('Error! missing parameter [{0}]'.format(param));
    }
};


function percentage(obj){
    var percent = 100 * obj.word_list.length / obj.total_words;
    percent = (percent).toFixed() - 100;
    return $('.progress .bar')
        .html(-percent + '%')
        .css('width', -percent + '%');
}


var Point = {
    negative: function () {
        var type = $('html').attr('id');
        Point.request('/api/stats/n/' + type)
    },
    positive: function () {
        var type = $('html').attr('id');
        Point.request('/api/stats/p/' + type)
    },
    speed: function(speed) {
    // TODO: calculations for word/minute section
        var type = $('html').attr('id');
        Point.request('/api/stats/p/' + type)
    },
    request: function(path) {
        $.ajax({
            url: path,
            success: function (output) {

            }
        });
    }
};

var isPage = function(page_name, callback) {
    if(page_name == $('html').attr('id')){
        return callback();
    }
    if(page_name == '*'){
        return callback();
    }
};

// Sound Settings
var Sound = {
    lib: '',
    init: function(){

    },
    metro_init: function(){
        // var gameMusic = new BandJS();

        // gameMusic.setTimeSignature(2, 2);
        // gameMusic.setTempo(180);

        // var rightHand = gameMusic.createInstrument('sine', 'oscillators');

        // rightHand.note('sixteenth', 'E3, F#2');
        // rightHand.finish();
        // Sound.lib = gameMusic;
    },
    metronome: function() {
        // if(docCookies.getItem('volume') == 'on'){
        //     Sound.lib.play();
        // }
    }
};

Sound.metro_init();


//Text Speed
function TextSpeed(speed) {
//    var result = speed * 60;
//    return $('.info strong').html(result);
}

// Set Level
$('#speed').find('a').click(function () {
    var speed = $(this).attr('data-speed');
    docCookies.setItem('speed', speed);
    $('#speed').find('.tick').html(speed);
    window.location.reload();
});

// Set Level
$('#level').find('a').click(function () {
    var level = $(this).attr('data-level');
    docCookies.setItem('level', level);
    $('#level').find('.tick').html(level);
    window.location.reload();
});


// Highchart Config
Highcharts.setOptions({
    lang: {
        weekdays: ['Pazar', 'Pazartesi', 'Salı', 'Çarşamba', 'Perşembe', 'Cuma', 'Cumartesi'],
        shweekdays: ['Paz', 'Pzt', 'Sal', 'Çar', 'Per', 'Cum', 'Cmt'],
        months: ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
        shortMonths: ['Oca', 'Şub', 'Mar', 'Nis', 'May', 'Haz', 'Tem', 'Ağu', 'Eyl', 'Eki', 'Kas', 'Ara'],
        downloadJPEG: 'JPG Olarak Kaydet',
        downloadPDF: 'PDF Olarak Kaydet',
        downloadPNG: 'PNG Olarak Kaydet',
        downloadSVG: 'SVG Olarak Kaydet',
        loading: 'Yükleniyor...',
        printChart: 'Raporu Yazdır',
        rangeSelectorFrom: '',
        rangeSelectorTo: 'ile',
        resetZoom: 'Zoom\'u Sıfırla'
    }
});


var getQueryString = function (name) {
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

var homework_counter = function(){
    if(getQueryString('homework') == 'None'){}else{
        $('.game-area .menu').html('');
        
        var minute = 1;
        // var minute = parseInt(docCookies.getItem('minute'), 10);
        
        function doneHomeWork(){
            var work_id = getQueryString('homework');
            $.ajax({ url: '/homework_done/' + work_id });
            
            docCookies.removeItem('speed', '/');
            docCookies.removeItem('level', '/');
            docCookies.removeItem('minute', '/');

        }
        
        $('.game-area .menu').countdown({
            until: '+' + minute + 'm',
            compact: true,
            onExpiry: doneHomeWork
        });

    }
}


var beginningOfTime = new Date();

var reading_counter = function (type) {
    $('a').click(function(e) {
        e.preventDefault();
        var href = $(this).attr('href');
        var endOfTime = new Date();
        var timeSpent=Math.abs( endOfTime - beginningOfTime );
        $.ajax({
            url: '/practices/record_time/' + type + '/',
            data: {
                time: timeSpent
            },
            success: function(){
                window.location.href = href;
            }
        });
    });
}