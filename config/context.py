#!/usr/bin/env python
# -*- coding: utf-8 -*-
import datetime

from config import settings


def get_user_days(request):
    start_date = request.user.profile.start_date
    date_diff = start_date - datetime.date.today()
    return - date_diff.days + 1


def variables(request):
    user_days = ''

    if hasattr(request.user, 'profile'):
        if request.user.profile.start_date:
            user_days = get_user_days(request)
        else:
            user_days = 'No start'
    else:
        user_days = 'no profile'
        
    return {
        'SITE_NAME': settings.SITE_NAME,
        'DESCRIPTION': settings.DESCRIPTION,
        'AUTHOR': settings.AUTHOR,
        'user_days': user_days
    }