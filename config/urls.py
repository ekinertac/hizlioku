#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url, static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from apps.quiz.views import graph

from config.utils import easy_url as easy
from config import settings

admin.autodiscover()

urlpatterns = patterns('',
    #main
    url(r'^$', 'config.views.landing', name='landing'),
    url(easy('^index/'), 'config.views.index', name='index'),
)

# header links
urlpatterns += (
    url(easy('^hizliokuma_nedir/'),     'config.views.hizliokuma_nedir',    name='hizliokuma_nedir'),
    url(easy('^egitim_sistemi/'),       'config.views.egitim_sistemi',      name='egitim_sistemi'),
)

urlpatterns += (
    # tachistoscope
    url(easy('^practices/tachistoscope/'),           'apps.practices.views.tachistoscope',           name='tachistoscope'),
    url(easy('^practices/tachistoscope/:level/'),    'apps.practices.views.tachistoscope',           name='tachistoscope_detail'),
    # env_focus
    url(easy('^practices/env_focus/'),            'apps.practices.views.env_focus',   name='env_focus'),
    url(easy('^practices/env_focus/:level/'),     'apps.practices.views.env_focus',   name='env_focus_detail'),
    # shadowing
    url(easy('^practices/shadowing/'),                      'apps.practices.views.shadowing',             name='shadowing'),
    url(easy('^practices/shadowing/:category_id/'),         'apps.practices.views.shadowing',             name='shadowing_category'),
    url(easy('^practices/shadowing_detail/:article_id/'),   'apps.practices.views.shadowing_detail',      name='shadowing_detail'),
    # grouping
    url(easy('^practices/grouping/'),                     'apps.practices.views.grouping',                name='grouping'),
    url(easy('^practices/grouping/:category_id/'),        'apps.practices.views.grouping',                name='grouping_category'),
    url(easy('^practices/grouping_detail/:article_id/'),  'apps.practices.views.grouping_detail',         name='grouping_detail'),
    # multi_focus
    url(easy('^practices/multi_focus/'),              'apps.practices.views.multi_focus', name='multi_focus'),
    url(easy('^practices/multi_focus/:level/'),       'apps.practices.views.multi_focus', name='multi_focus_detail'),
    # block_reading
    url(easy('^practices/block_reading/'),                        'apps.practices.views.block_reading',           name='block_reading'),
    url(easy('^practices/block_reading/:category_id/'),           'apps.practices.views.block_reading',           name='block_reading_category'),
    url(easy('^practices/block_reading_detail/:article_id/'),     'apps.practices.views.block_reading_detail',    name='block_reading_detail'),
    # eye_focusing
    url(easy('^practices/eye_focusing/'),            'apps.practices.views.eye_focusing',            name='eye_focusing'),
    url(easy('^practices/eye_focusing/:level/'),     'apps.practices.views.eye_focusing',            name='eye_focusing_detail'),

    # eye_muscle_development
    url(easy('^practices/eye_muscle_development/'),  'apps.practices.views.eye_muscle_development', name='eye_muscle_development'),

    # speed_reading
    url(easy('^quiz/'),                         'apps.quiz.views.category', name='quiz'),
    url(easy('^quiz/category/:category_id/'),   'apps.quiz.views.test', name='quiz_cat'),
    url(easy('^quiz/detail/:test_id/'),         'apps.quiz.views.detail', name='quiz_test'),
    url(easy('^quiz/questions/:test_id/'),      'apps.quiz.views.questions', name='quiz_questions'),
    url(easy('^quiz/result/'),                  'apps.quiz.views.result', name='quiz_result'),

    # Quiz Users
    url(easy('^exam/'), 'apps.exam.views.categories', name='exam'),
    url(easy('^exam/:category_id/'), 'apps.exam.views.categories', name='exam_category'),
    url(easy('^exam_detail/:exam_id/'), 'apps.exam.views.detail', name='exam_detail'),

    # HomeWork
    url(easy('^homework/'),          'apps.homework.views.homework_index',   name='homework_index'),
    url(easy('^homework/:day/'),     'apps.homework.views.homework_day',     name='homework_day'),
    url(easy('^homework_user'),      'apps.homework.views.user_works',       name='homework_user'),
    url(easy('^homework_done/:id/'), 'apps.homework.views.done',             name='homework_done'),

    # Presentations
    url(easy('^presentations/'), 'apps.presentations.views.index', name='presentations'),

    # performance
    url(easy('^practices/performance/'),             'apps.practices.views.performance', name='performance'),
    url(easy('^practices/performance/:event_type/'), 'apps.practices.views.performance', name='performance'),


    url(easy('^practices/record_time/:event_type/'), 'apps.practices.views.record_time', name='record_time'),
)

# api
urlpatterns += (
    url(easy('^api/get_articles/'),                             'config.api.get_articles',      name="api_get_articles"),
    url(easy('^api/stats/:status/:stat_type/'),                 'apps.practices.views.stats',   name='api_stats'),
    url(easy('^api/test_read_finish/:total_time/:word_count/'), 'apps.quiz.views.finish',       name='api_test_read_finish'),
    url(easy('^api/test_graph/'),                               'apps.quiz.views.graph',      name='api_test_graph'),
)

# Authentication
urlpatterns += (
    url(r'^admin/', include(admin.site.urls)),
    url(easy('^accounts/login/'), 'apps.profiles.views.auth_login', name='login'),
    url(easy('^accounts/logout/'), 'apps.profiles.views.auth_logout', name='logout'),
    url(easy('^accounts/disabled/'), 'apps.profiles.views.disabled_account', name='disabled_account'),
    url(easy('^accounts/change_password/'), 'apps.profiles.views.change_password', name='change_password'),
)

# Media Urls
urlpatterns += (
    static.static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
)