#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re
import time


def easy_url(url_str):
    pattern = re.compile(r':\w+/?')
    matches = pattern.findall(url_str)
    for match in matches:
        var = match[1:-1]
        var_re = r'(?P<%s>.*)/' % var
        url_str = url_str.replace(match, var_re)
    url_str += '$'
    return url_str


def tc_kimlik_verify(data):
    # Veriyi uzunluğunu kontrol edilecek.
    if len(data) != 11:
        # Girmiş olduğunuz numara 11 haneden oluşmak zorundadır!
        return False
    else:
    # İlk hane sıfırdan farklı olmak zorunda kontrol edilecek.
        if int(data[0]) == 0:
            # İlk hane sıfır (0) olamaz!
            return False
        else:
            # 1. 3. 5. 7. 9. hanelerin toplamının 7 katından 2.4.6.8. hanelerin
            # toplamı çıkartılıp 10'a bölündüğünde; kalan 10. haneyi vermeli.
            if ((((int(data[0]) + int(data[2]) + int(data[4]) + int(data[6]) + int(data[8])) * 7) - ((int(data[1]) + int(data[3]) + int(data[5]) + int(data[7])))) % 10) != int(data[9]):
                # 10. hane doğrulanamadı!
                return False
            else:
                # İlk 10 hanenin toplamının 10 ile bölümünden kalan 11. haneyi vermelidir.
                a = 0
                for i in data[0:10]:
                    a += int(i)
                if (a % 10) != int(data[10]):
                    # 11. hane doğrulanamadı.
                    return False
                else:
                    # TC Kimlik no doğrulama başarılı.
                    return True


def make_epoch(date):
    return int(time.mktime(date.timetuple()) * 1000) + 86400000
