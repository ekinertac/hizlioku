#!/usr/bin/env python
# -*- coding: utf-8 -*-

LANG_CHOICES = (
    ('tr', u'Türkçe'),
    ('en', u'English'),
)

KIND_CHOICES = (
    ('teacher', u'Eğitmen'),
    ('student', u'Öğrenci'),
)


TYPE_CHOICES = (
    ('y', u'Yetişkin'),
    ('s', u'Sınav'),
    ('i', u'İlkokul'),
)

GAME_CHOICES = (
    ('tachistoscope', u'Takistoskop'),
    ('env_focus', u'Çevre Fokus'),
    ('multi_focus', u'Multi Fokus'),
    ('eye_focus', u'Göz Odaklama'),
    ('shadowing', u'Gölgeleme'),
    ('grouping', u'Gruplama'),
    ('block_reading', u'Blok Okuma'),
)

WORK_TYPES = (
    ('tachistoscope', u'Takistoskop'),
    ('env_focus', u'Çevre Fokus'),
    ('multi_focus', u'Multi Fokus'),
    ('eye_focusing', u'Göz Odaklama'),

    ('shadowing', u'Gölgeleme'),
    ('grouping', u'Gruplama'),
    ('block_reading', u'Blok Okuma'),
)


def level_choices():
    arr = []
    for item in range(1, 11):
        arr.append(tuple([item, 'Seviye %s' % item]))
    return tuple(arr)


def day_choices():
    arr = []
    for item in range(1, 31):
        arr.append(tuple([item, u'%s. Gün' % item]))
    return tuple(arr)


def speed_choices():
    arr = []
    for item in range(1, 13):
        arr.append(tuple([item, u'Hız %s' % item]))
    return tuple(arr)
