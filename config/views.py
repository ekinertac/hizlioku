#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib.auth import logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.shortcuts import redirect
from annoying.decorators import render_to
from apps.corps.models import CorpsModel
from apps.profiles.models import ProfileModel

import json


@login_required()
@render_to('index.html')
def index(request):
    return {}


@render_to('landing.html')
def landing(request):
    # f = open('kultur_ders.json')
    # data = json.loads(f.read())
    # subeler = []
    # idx = 0
    # for item in data:
    #     corp = CorpsModel.objects.get_or_create(title=item['SUBESI'])

    #     username = item['KULLANICI ADI']
    #     password = '12345'
    #     first = item['ADI']
    #     last = item['SOYADI']

    #     user = User()
    #     user.username = username
    #     user.set_password(password)
    #     user.first_name = first
    #     user.last_name = last
    #     user.is_active = True
    #     user.save()

    #     profile = ProfileModel()
    #     profile.user = user
    #     profile.type = 's'
    #     profile.lang = 'tr'

    #     profile.corporate = corp[0]

    #     profile.kind = 'student'
    #     profile.save()
        
    #     idx += 1
    #     print '%s - %s' % (user, idx)

    return {}

@render_to('pages/hizliokuma_nedir.html')
def hizliokuma_nedir(request):
    return {}

@render_to('pages/egitim_sistemi.html')
def egitim_sistemi(request):
    return {}


@login_required
def logout_page(request):
    auth_logout(request)
    return redirect('/')