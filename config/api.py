#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
from annoying.decorators import JsonResponse

from apps.practices.models import ArticleModel

def get_articles(request):
    lang = request.session['language']
    type = request.session['user_type']
    category = request.POST['category_id']
    articles = ArticleModel.objects.filter(lang=lang, type=type, category=category)
    response = []

    for article in articles:
        response.append({
            'article_id' : article.id,
            'article_title' : article.title
        })

    return JsonResponse({
        'data': response
        })