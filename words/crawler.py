#!/usr/bin/env python
# -*- coding: utf-8 -*-

from BeautifulSoup import BeautifulSoup
import json
import Levenshtein


# def collect_words():
#     # source : tr.wiktionary.org/wiki/Vikisözlük:Sözcük_listesi_(A)
#     html = open('html/a.html', 'r')
#     soup = BeautifulSoup(html.read())
#
#     elements = soup.findAll('a')
#
#     data = []
#     for a in elements:
#         data.append(a.text)
#
#     words = []
#
#     for word in data:
#         if word.find(' ') > 0:
#             words.append(word.split(' ')[0])
#         else:
#             words.append(word)
#
#     words = list(set(words))
#
#
#     txt = open('txt/a.txt', 'a')
#     for w in words:
#         txt.write('%s,' % w.lower().encode('utf8'))


# word_list = open('words.txt', 'r')
#
# words = word_list.read().split(',')
#
# words = list(set(words))
#
#
# seperated = {
#     'level_1': [],
#     'level_2': [],
#     'level_3': [],
#     'level_4': [],
#     'level_5': [],
#     'level_6': [],
#     'level_7': [],
#     'level_8': [],
#     'level_9': [],
#     'level_10': []
# }
#
# for word in words:
#     if len(word) == 2 or len(word) == 3:
#         seperated['level_1'].append(word)
#
#     if len(word) == 3 or len(word) == 4:
#         seperated['level_2'].append(word)
#
#     if len(word) == 4 or len(word) == 5:
#         seperated['level_3'].append(word)
#
#     if len(word) == 5 or len(word) == 6:
#         seperated['level_4'].append(word)
#
#     if len(word) == 6 or len(word) == 7:
#         seperated['level_5'].append(word)
#
#     if len(word) == 7 or len(word) == 8:
#         seperated['level_6'].append(word)
#
#     if len(word) == 8 or len(word) == 9:
#         seperated['level_7'].append(word)
#
#     if len(word) == 9 or len(word) == 10:
#         seperated['level_8'].append(word)
#
#     if len(word) == 10 or len(word) == 11:
#         seperated['level_9'].append(word)
#
#     if len(word) == 11 or len(word) == 12:
#         seperated['level_10'].append(word)
#
# js = open('words.json', 'w')
#
# js.write(json.dumps(seperated))

words = open('words.json', 'r')
words = json.loads(words.read())

asorted = {
    'level_1': [],
    'level_2': [],
    'level_3': [],
    'level_4': [],
    'level_5': [],
    'level_6': [],
    'level_7': [],
    'level_8': [],
    'level_9': [],
    'level_10': []
}

for level in words:
    for word in sorted(words[level]):
        asorted[level].append(word)

sorted_f = open('sorted.json', 'w')
sorted_f.write(json.dumps(asorted))