#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models


class PresentationModel(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'Sunum Başlığı')
    pdf_file = models.FileField(upload_to='presentations/', verbose_name=u'PDF Dosyası')
    desc = models.CharField(max_length=255, verbose_name=u'Sunum Açıklaması')

    class Meta:
        verbose_name = 'Sunum'
        verbose_name_plural = 'Sunumlar'

    def __unicode__(self):
        return '%s' % self.title
