from django.contrib import admin
from apps.presentations.models import PresentationModel

admin.site.register(PresentationModel)