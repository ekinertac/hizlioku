# Create your views here.
from annoying.decorators import render_to
from apps.presentations.models import PresentationModel


@render_to('presentations/index.html')
def index(request):

    slides = PresentationModel.objects.all()

    return {
        'slides': slides
    }