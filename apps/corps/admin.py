#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from apps.corps.models import CorpsModel

admin.site.register(CorpsModel)

from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

# class ProfileAcc(UserAdmin):
# 	list_display = ['username', 'date_joined']
# 	list_filter = [ 'date_joined']

# admin.site.unregister(User)
# admin.site.register(User, ProfileAcc)