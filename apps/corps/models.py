#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models


class CorpsModel(models.Model):
    title = models.CharField(max_length=255, verbose_name=u'Kurum Adı')
    phone = models.CharField(max_length=255, verbose_name=u'Iletişim Telefonu',
                             help_text=u'0xxx xxx xxxx şeklinde giriş yapınız.', blank=True)
    email = models.EmailField(blank=True)
    address = models.TextField(blank=True)

    class Meta:
        verbose_name = 'Kurum'
        verbose_name_plural = 'Kurumlar'

    def __unicode__(self):
        return '%s' % self.title