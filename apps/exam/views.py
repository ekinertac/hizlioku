from annoying.decorators import render_to
from django.contrib.auth.decorators import login_required
from apps.quiz.models import *


@login_required
@render_to('exam/categories.html')
def categories(request, category_id=None):
    cats = QuizCategoryModel.objects.filter(type='s')

    if category_id:
        articles = QuizModel.objects.filter(categories__id=category_id)

        return {
            'categories': cats,
            'articles': articles
        }
    else:
        return {
            'categories': cats
        }


@login_required
@render_to('exam/detail.html')
def detail(request, exam_id):
    questions = QuizQuestionModel.objects.filter(quiz__id=exam_id)
    return {
        'questions': questions,
        'test_id' : exam_id
    }