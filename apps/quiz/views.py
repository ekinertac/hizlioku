import datetime, time
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from annoying.decorators import render_to, JsonResponse
import json

from apps.quiz.models import *
from config.utils import make_epoch


@render_to('apps/speed_reading.html')
def category(request):
    lang = request.session['language']
    user_type = request.session['user_type']

    category = QuizCategoryModel.objects.filter(lang=lang, type=user_type)

    return {
        'categories' : category,
        # 'tests': tests
    }


@render_to('apps/speed_reading.html')
def test(request, category_id):
    lang = request.session['language']
    user_type = request.session['user_type']
    category = QuizCategoryModel.objects.filter(lang=lang, type=user_type)
    tests = QuizModel.objects.filter(categories__id=category_id)

    return {
        'tests': tests,
        'categories': category,
    }


@render_to('apps/speed_reading_detail.html')
def detail(request, test_id):
    test = QuizModel.objects.get(pk=test_id)
    return {
        'test': test
    }


@render_to('apps/speed_reading_tests.html')
def questions(request, test_id):
    questions = QuizQuestionModel.objects.filter(quiz__id=test_id)

    return {
        'questions': questions,
        'test_id': test_id,
        'total_ms': request.session.get('total_ms', False),
        'word_count': request.session.get('word_count', False)
    }


@render_to('apps/speed_reading_result.html')
def result(request):

    if request.POST:
        post = dict(request.POST.iteritems())
        post.pop('csrfmiddlewaretoken')

        correct_answer = 0
        test_id = post.pop('test_id')
        total_ms = int(post.pop('total_ms')) / 1000
        word_count = int(post.pop('word_count'))

        speed = (60 * word_count) / total_ms

        for key, val in post.iteritems():
            answer = QuizAnswerModel.objects.get(pk=val)
            if answer.status:
                correct_answer += 1

        score = QuizScoreModel(
            profile=request.user,
            quiz=QuizModel.objects.get(pk=test_id),
            correct_answers=correct_answer,
            date=datetime.datetime.now(),
            reading_speed=speed
        )

        score.save()

    results = QuizScoreModel.objects.filter(date__range=[request.user.profile.start_date, datetime.datetime.now()])

    paginator = Paginator(results, 20)

    page = request.GET.get('page')

    try:
        scores = paginator.page(page)
    except PageNotAnInteger:
        scores = paginator.page(1)
    except EmptyPage:
        scores = paginator.page(paginator.num_pages)

    return {
        'results': scores,
        'page_range': scores.paginator.page_range,
        'num_pages': scores.paginator.num_pages,
    }


def graph(request):
    results = QuizScoreModel.objects\
        .filter(date__range=[request.user.profile.start_date, datetime.datetime.now()])\
        .order_by('id')

    graph = {
        'question': [],
        'speed': []
    }

    for result in results:

        percent = (result.correct_answers * 100) / result.total_question
        date = make_epoch(result.date)
        graph['question'].append([date, percent])
        graph['speed'].append([date, result.reading_speed])

    return JsonResponse(graph)


def finish(request, total_time, word_count):
    request.session['total_ms'] = total_time
    request.session['word_count'] = word_count

    return JsonResponse({
        'total_time': total_time,
        'word_count': word_count
    })