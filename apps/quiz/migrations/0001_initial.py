# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'QuizCategoryModel'
        db.create_table(u'quiz_quizcategorymodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lang', self.gf('django.db.models.fields.CharField')(default='tr', max_length=255)),
            ('type', self.gf('django.db.models.fields.CharField')(default='y', max_length=255)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'quiz', ['QuizCategoryModel'])

        # Adding model 'QuizModel'
        db.create_table(u'quiz_quizmodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('text', self.gf('django.db.models.fields.TextField')()),
            ('status', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('created', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
            ('modified', self.gf('django.db.models.fields.DateTimeField')(auto_now=True, blank=True)),
        ))
        db.send_create_signal(u'quiz', ['QuizModel'])

        # Adding M2M table for field categories on 'QuizModel'
        m2m_table_name = db.shorten_name(u'quiz_quizmodel_categories')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('quizmodel', models.ForeignKey(orm[u'quiz.quizmodel'], null=False)),
            ('quizcategorymodel', models.ForeignKey(orm[u'quiz.quizcategorymodel'], null=False))
        ))
        db.create_unique(m2m_table_name, ['quizmodel_id', 'quizcategorymodel_id'])

        # Adding model 'QuizQuestionModel'
        db.create_table(u'quiz_quizquestionmodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('quiz', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['quiz.QuizModel'])),
            ('question', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'quiz', ['QuizQuestionModel'])

        # Adding model 'QuizAnswerModel'
        db.create_table(u'quiz_quizanswermodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['quiz.QuizQuestionModel'])),
            ('answer', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'quiz', ['QuizAnswerModel'])

        # Adding model 'QuizScoreModel'
        db.create_table(u'quiz_quizscoremodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('profile', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('quiz', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['quiz.QuizModel'])),
            ('correct_answers', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('date', self.gf('django.db.models.fields.DateTimeField')()),
            ('reading_speed', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'quiz', ['QuizScoreModel'])


    def backwards(self, orm):
        # Deleting model 'QuizCategoryModel'
        db.delete_table(u'quiz_quizcategorymodel')

        # Deleting model 'QuizModel'
        db.delete_table(u'quiz_quizmodel')

        # Removing M2M table for field categories on 'QuizModel'
        db.delete_table(db.shorten_name(u'quiz_quizmodel_categories'))

        # Deleting model 'QuizQuestionModel'
        db.delete_table(u'quiz_quizquestionmodel')

        # Deleting model 'QuizAnswerModel'
        db.delete_table(u'quiz_quizanswermodel')

        # Deleting model 'QuizScoreModel'
        db.delete_table(u'quiz_quizscoremodel')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'quiz.quizanswermodel': {
            'Meta': {'object_name': 'QuizAnswerModel'},
            'answer': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['quiz.QuizQuestionModel']"}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'})
        },
        u'quiz.quizcategorymodel': {
            'Meta': {'object_name': 'QuizCategoryModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'})
        },
        u'quiz.quizmodel': {
            'Meta': {'ordering': "('-id',)", 'object_name': 'QuizModel'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['quiz.QuizCategoryModel']", 'symmetrical': 'False', 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'auto_now': 'True', 'blank': 'True'}),
            'status': ('django.db.models.fields.CharField', [], {'max_length': '10'}),
            'text': ('django.db.models.fields.TextField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'})
        },
        u'quiz.quizquestionmodel': {
            'Meta': {'object_name': 'QuizQuestionModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.TextField', [], {}),
            'quiz': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['quiz.QuizModel']"})
        },
        u'quiz.quizscoremodel': {
            'Meta': {'ordering': "['-id']", 'object_name': 'QuizScoreModel'},
            'correct_answers': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'date': ('django.db.models.fields.DateTimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'profile': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"}),
            'quiz': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['quiz.QuizModel']"}),
            'reading_speed': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['quiz']