# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models
from config.constants import LANG_CHOICES, TYPE_CHOICES


class QuizCategoryModel(models.Model):
    lang = models.CharField('Dil', max_length=255, choices=LANG_CHOICES, default='tr')
    type = models.CharField('Öğrenci Tipi', max_length=255, choices=TYPE_CHOICES, default='y')
    title = models.CharField(max_length=255, verbose_name=u'Kategori')

    class Meta:
        verbose_name = u'Test Kategorisi'
        verbose_name_plural = u'Test Kategorileri'


    def __unicode__(self):
        return u'{0} - {1} - {2}'.format(self.get_lang_display(), self.get_type_display(), self.title)


class QuizModel(models.Model):

    STATUS_CHOICES = (
        ('draft', u'Taslak'),
        ('public', u'Görünür'),
        ('closed', u'Görünmez'),
    )

    title = models.CharField(verbose_name=u'Test Başlığı', max_length=255)
    text = models.TextField(verbose_name=u'Metin', blank=True)
    status = models.CharField(choices=STATUS_CHOICES, max_length=10, verbose_name=u'Durumu')
    categories = models.ManyToManyField(QuizCategoryModel, blank=True)

    created = models.DateTimeField(auto_now_add=True)
    modified = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u'Test'
        verbose_name_plural = u'Testler'

        ordering = ('-id',)

    def __unicode__(self):
        return u'%s' % self.title

    @property
    def question_count(self):
        return QuizQuestionModel.objects.filter(quiz=self).count()

    @property
    def is_published(self):
        if self.status is 'public':
            return True


class QuizQuestionModel(models.Model):
    quiz = models.ForeignKey(QuizModel, verbose_name=u'Test')
    question = models.TextField(verbose_name=u'Soru')

    class Meta:
        verbose_name = u'Soru'
        verbose_name_plural = u'Sorular'


class QuizAnswerModel(models.Model):
    question = models.ForeignKey(QuizQuestionModel)
    answer = models.CharField(max_length=255, verbose_name=u'Cevap')
    status = models.BooleanField(default=False, verbose_name=u'Doğru mu ?')

    class Meta:
        verbose_name = u'Cevap'
        verbose_name_plural = u'Cevaplar'


    def __unicode__(self):
        return u'%s' % self.answer


class QuizScoreModel(models.Model):
    profile = models.ForeignKey(User, verbose_name=u'Kullanıcı')
    quiz = models.ForeignKey(QuizModel, verbose_name=u'Test')
    correct_answers = models.IntegerField(default=0, verbose_name=u'Doğru cevap sayısı')
    date = models.DateTimeField(verbose_name=u'Tarih')
    reading_speed = models.IntegerField(verbose_name='Okuma Hızı')

    class Meta:
        verbose_name = u'Sonuç'
        verbose_name_plural = u'Sonuçlar'
        ordering = ['-id',]

    @property
    def total_question(self):
        return self.quiz.question_count

    def __unicode__(self):
        return u'%s - %s' % (self.profile, self.quiz)



