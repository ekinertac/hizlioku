from django.contrib import admin
from nested_inlines.admin import NestedModelAdmin, NestedStackedInline, NestedTabularInline
from apps.quiz.models import *


admin.site.register(QuizCategoryModel)


class AnswerNested(NestedTabularInline):
    model = QuizAnswerModel
    extra = 5
    max_num = 5


class QuestionNestedAdmin(NestedStackedInline):
    model = QuizQuestionModel
    inlines = [AnswerNested, ]
    extra = 10


class QuizNestedAdmin(NestedModelAdmin):
    inlines = [QuestionNestedAdmin, ]
    save_on_top = True


admin.site.register(QuizModel, QuizNestedAdmin)


admin.site.register(QuizQuestionModel)


class QuizScoreAdmin(admin.ModelAdmin):
    list_display = ['profile', 'quiz', 'correct_answers', 'date', 'reading_speed']
    save_as = True
    list_filter = ['profile',]

    def __init__(self, *args, **kwargs):
        super(QuizScoreAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )


admin.site.register(QuizScoreModel, QuizScoreAdmin)
