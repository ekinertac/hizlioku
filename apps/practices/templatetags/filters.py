from django import template
from django.template.defaultfilters import stringfilter
from django.utils.html import conditional_escape, escape
from django.utils.safestring import mark_safe, SafeData
from django.utils.text import normalize_newlines

register = template.Library()

@register.filter(is_safe=True, needs_autoescape=True)
@stringfilter
def linebreakbr(value, autoescape=None):
    """
    Converts all newlines in a piece of plain text to HTML line breaks
    (``<br/>``).
    """
    autoescape = autoescape and not isinstance(value, SafeData)
    value = normalize_newlines(value)
    if autoescape:
        value = escape(value)
    return mark_safe(value.replace('\n', '<br/>'))