#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User

from django.db import models
from django.db.models.options import get_verbose_name
from django.utils.safestring import mark_safe

from sorl.thumbnail import ImageField

from apps.profiles.models import ProfileModel
from config.constants import LANG_CHOICES, TYPE_CHOICES, GAME_CHOICES


class TachistoscopeModel(models.Model):
    lang = models.CharField('Language', max_length=255, choices=LANG_CHOICES, default='tr')
    type = models.CharField('Öğrenci Tipi', max_length=255, choices=TYPE_CHOICES, default='y')
    title = models.CharField(max_length=255)
    level = models.IntegerField(default=0)
    words = models.TextField(help_text=mark_safe(u"""
        <strong>Kayıt Örnegi:</strong><br>
        <code>
        aegis,reeda,aqaba
        </code>
        """))
    point = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Takistiskop Kelime'
        verbose_name_plural = 'Takistiskop Kelimeler'
        ordering = ['level']

    def __unicode__(self):
        return u"{0}".format(self.title)

    def get_absolute_url(self):
        return '/practices/tachistoscope/%s' % self.level


class EnvFocusModel(models.Model):
    lang = models.CharField('Language',max_length=255, choices=LANG_CHOICES, default='tr')
    type = models.CharField('Öğrenci Tipi', max_length=255, choices=TYPE_CHOICES, default='y')
    title = models.CharField(max_length=255)
    level = models.IntegerField(default=0)
    words = models.TextField(default="", help_text=mark_safe("""
        <strong>Kayit Ornegi:</strong><br>
        <code>
        aegis,reeda,aqaba
        </code>
        """))

    point = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Çevre Fokus Kelime'
        verbose_name_plural = 'Çevre Fokus Kelimeler'
        ordering = ['level']

    def __unicode__(self):
        return u"%s" % self.title

    def get_absolute_url(self):
        return '/practices/env_focus/%s' % self.level


class ArticleCategoryModel(models.Model):
    lang = models.CharField('Language',max_length=255, choices=LANG_CHOICES, default='tr')
    type = models.CharField('Öğrenci Tipi', max_length=255, choices=TYPE_CHOICES, default='y')
    title = models.CharField(max_length=255)

    class Meta:
        verbose_name = 'Makale Kategori'
        verbose_name_plural = 'Makale Kategorisi'
        get_latest_by = ['-id']

    def __unicode__(self):
        return u"%s" % self.title


class ArticleModel(models.Model):
    CATEGORY_CHOICES = (
        ('shadowing', 'Gölgeleme'),
        ('grouping', 'Gruplama'),
        ('block_reading', 'Blok Okuma'),
    )
    lang = models.CharField('Language',max_length=255, choices=LANG_CHOICES, default='tr')
    type = models.CharField('Öğrenci Tipi', max_length=255, choices=TYPE_CHOICES, default='y')
    category = models.ForeignKey(ArticleCategoryModel)
    kind = models.CharField('Makale Tipi', choices=CATEGORY_CHOICES, max_length=255, default='shadowing')
    title = models.CharField(max_length=255)

    text = models.TextField(default=" ", help_text=mark_safe(u"""
        Satır atlamak icin <code>&lt;br&gt;</code> kodunu kullaniyoruz
    """))

    seperators = models.TextField(default=" ")
    point = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Makale'
        verbose_name_plural = 'Makaleler'
        ordering = ['id']

    def __unicode__(self):
        cat = self.category
        tit = u"%s" % self.title
        result = u"%s - %s" % (cat, tit)
        return result


class MultiFocusModel(models.Model):
    lang = models.CharField('Language' , max_length=255, choices=LANG_CHOICES, default='tr')
    type = models.CharField('Öğrenci Tipi', max_length=255, choices=TYPE_CHOICES, default='y')
    title = models.CharField(max_length=255)
    level = models.IntegerField(default=0)
    words = models.TextField(default="'kelime|kelime', 'kelime|kalima'", help_text=mark_safe("""
        <strong>Kayit Ornegi:</strong><br>
        <code>'kelime|kelime', 'kelime|kalima'</code>
    """))
    point = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Multi Focus Kelimesi'
        verbose_name_plural = 'Multi Focus Kelimeleri'
        ordering = ['level']

    def __unicode__(self):
        return u"%s" % self.title

    def get_absolute_url(self):
        return '/practices/multi_focus/%s' % self.level


class EyeFocusModel(models.Model):
    lang = models.CharField('Language',max_length=255, choices=LANG_CHOICES, default='tr')
    type = models.CharField('Öğrenci Tipi', max_length=255, choices=TYPE_CHOICES, default='y')
    title = models.CharField(max_length=255)
    level = models.IntegerField(default=0)
    image = ImageField(upload_to='images/eyefocus/')
    positions = models.TextField(default="[5,5], [90,5], [5,90], [90,90]", help_text=mark_safe("""
        <div class="help_text">
        <p><strong>Kayir Ornegi:</strong></p>
        <p><code>[pozisyon_top, pozisyon_left], [5,5], [90,5], [5,90], [90,90]</code></p>
        <p><strong>Maksimum:</strong> 90</p>
        <p><strong>Minimum:</strong> 5</p>
        </div>
        <div class="grid"></div>
    """))
    point = models.IntegerField(default=0)

    class Meta:
        verbose_name = 'Göz Odaklama'
        verbose_name_plural = 'Göz Odaklama'
        ordering = ['level']

    def __unicode__(self):
        return u"%s" % self.title

    def get_absolute_url(self):
        return '/practices/eye_focusing/%s' % self.level


class GameStatsModel(models.Model):
    user = models.ForeignKey(User)
    type = models.CharField(max_length=255, choices=GAME_CHOICES)
    status = models.BooleanField(default=False)
    point = models.IntegerField(default=0)
    date = models.DateTimeField()

    class Meta:
        verbose_name = 'Istatistik'
        verbose_name_plural = 'Istatistikler'

    def __unicode__(self):
        if self.status:
            return "Added Positive on {0} to {1}".format(self.type, self.user)
        else:
            return "Added Negative on {0} to {1}".format(self.type, self.user)
