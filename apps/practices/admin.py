#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.contrib import admin
from apps.practices.models import *
from django.contrib.admin.util import flatten_fieldsets



def duplicate_event(self, request, queryset):
    for obj in queryset:
        obj.id = None
        obj.save()
duplicate_event.short_description = "Duplicate selected record"


class TachistoscopeModelAdmin(admin.ModelAdmin):
    list_display = ['__unicode__', 'lang', 'type']
    list_filter = ('lang', 'type')
    # list_editable = ['type']
    actions = [duplicate_event]
    save_as = True

admin.site.register(TachistoscopeModel, TachistoscopeModelAdmin)


class EnvFocusModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'lang', 'type']
    list_filter = ('lang', 'type')
    save_as = True

admin.site.register(EnvFocusModel, EnvFocusModelAdmin)


class MultiFocusModelAdmin(admin.ModelAdmin):
    list_display = ['title', 'lang', 'type']
    list_filter = ('lang', 'type')
    save_as = True

admin.site.register(MultiFocusModel, MultiFocusModelAdmin)


class EyeFocusModelAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'screen': ('/static/css/admin/gridSelector.css',)
        }
        js = (
            '/static/js/vendor/djangoConflict.js',
            '/static/js/vendor/jquery.js',
            '/static/js/vendor/underscore.js',
            '/static/js/admin/gridSelector.js'
        )

    list_display = ['title', 'lang', 'type', ]
    list_filter = ('lang', 'type')
    save_as = True

admin.site.register(EyeFocusModel, EyeFocusModelAdmin)


class ArticleCategoryAdminInline(admin.TabularInline):
    model = ArticleModel
    extra = 2


class ArticleCategoryAdmin(admin.ModelAdmin):
    list_display = ['title', 'lang', 'type']
    inlines = [ArticleCategoryAdminInline]


class ArticleAdmin(admin.ModelAdmin):
    list_display = ['title', 'kind', 'category', 'lang', 'type']
    list_filter = ('category', 'lang', 'type', 'kind')
    # list_editable = ['title',]
    search_fields = ['title',]
    save_as = True

admin.site.register(ArticleCategoryModel, ArticleCategoryAdmin)
admin.site.register(ArticleModel, ArticleAdmin)


class GameStatsAdmin(admin.ModelAdmin):
    list_display = ['user', 'date', 'type', 'status', 'point']
    search_fields = ['user', 'type']
    list_filter = ['date', 'type', 'status', 'user']
    list_editable = ['date', 'type', 'status', 'point']
    save_as = True
    actions = [duplicate_event]

    def __init__(self, *args, **kwargs):
        super(GameStatsAdmin, self).__init__(*args, **kwargs)
        self.list_display_links = (None, )

admin.site.register(GameStatsModel, GameStatsAdmin)