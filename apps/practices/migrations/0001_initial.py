# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'TachistoscopeModel'
        db.create_table(u'practices_tachistoscopemodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lang', self.gf('django.db.models.fields.CharField')(default='tr', max_length=255)),
            ('type', self.gf('django.db.models.fields.CharField')(default='y', max_length=255)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('level', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('words', self.gf('django.db.models.fields.TextField')()),
            ('point', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'practices', ['TachistoscopeModel'])

        # Adding model 'EnvFocusModel'
        db.create_table(u'practices_envfocusmodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lang', self.gf('django.db.models.fields.CharField')(default='tr', max_length=255)),
            ('type', self.gf('django.db.models.fields.CharField')(default='y', max_length=255)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('level', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('words', self.gf('django.db.models.fields.TextField')(default='')),
            ('point', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'practices', ['EnvFocusModel'])

        # Adding model 'ArticleCategoryModel'
        db.create_table(u'practices_articlecategorymodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lang', self.gf('django.db.models.fields.CharField')(default='tr', max_length=255)),
            ('type', self.gf('django.db.models.fields.CharField')(default='y', max_length=255)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'practices', ['ArticleCategoryModel'])

        # Adding model 'ArticleModel'
        db.create_table(u'practices_articlemodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lang', self.gf('django.db.models.fields.CharField')(default='tr', max_length=255)),
            ('type', self.gf('django.db.models.fields.CharField')(default='y', max_length=255)),
            ('category', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['practices.ArticleCategoryModel'])),
            ('kind', self.gf('django.db.models.fields.CharField')(default='shadowing', max_length=255)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('text', self.gf('django.db.models.fields.TextField')(default=' ')),
            ('seperators', self.gf('django.db.models.fields.TextField')(default=' ')),
            ('point', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'practices', ['ArticleModel'])

        # Adding model 'MultiFocusModel'
        db.create_table(u'practices_multifocusmodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lang', self.gf('django.db.models.fields.CharField')(default='tr', max_length=255)),
            ('type', self.gf('django.db.models.fields.CharField')(default='y', max_length=255)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('level', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('words', self.gf('django.db.models.fields.TextField')(default="'kelime|kelime', 'kelime|kalima'")),
            ('point', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'practices', ['MultiFocusModel'])

        # Adding model 'EyeFocusModel'
        db.create_table(u'practices_eyefocusmodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('lang', self.gf('django.db.models.fields.CharField')(default='tr', max_length=255)),
            ('type', self.gf('django.db.models.fields.CharField')(default='y', max_length=255)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('level', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('image', self.gf('sorl.thumbnail.fields.ImageField')(max_length=100)),
            ('positions', self.gf('django.db.models.fields.TextField')(default='[10,10], [350,10], [10,100], [350,100]')),
            ('point', self.gf('django.db.models.fields.IntegerField')(default=0)),
        ))
        db.send_create_signal(u'practices', ['EyeFocusModel'])

        # Adding model 'GameStatsModel'
        db.create_table(u'practices_gamestatsmodel', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profiles.ProfileModel'])),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('status', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('point', self.gf('django.db.models.fields.IntegerField')(default=0)),
            ('date', self.gf('django.db.models.fields.DateField')()),
        ))
        db.send_create_signal(u'practices', ['GameStatsModel'])


    def backwards(self, orm):
        # Deleting model 'TachistoscopeModel'
        db.delete_table(u'practices_tachistoscopemodel')

        # Deleting model 'EnvFocusModel'
        db.delete_table(u'practices_envfocusmodel')

        # Deleting model 'ArticleCategoryModel'
        db.delete_table(u'practices_articlecategorymodel')

        # Deleting model 'ArticleModel'
        db.delete_table(u'practices_articlemodel')

        # Deleting model 'MultiFocusModel'
        db.delete_table(u'practices_multifocusmodel')

        # Deleting model 'EyeFocusModel'
        db.delete_table(u'practices_eyefocusmodel')

        # Deleting model 'GameStatsModel'
        db.delete_table(u'practices_gamestatsmodel')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'practices.articlecategorymodel': {
            'Meta': {'object_name': 'ArticleCategoryModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'})
        },
        u'practices.articlemodel': {
            'Meta': {'object_name': 'ArticleModel'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['practices.ArticleCategoryModel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kind': ('django.db.models.fields.CharField', [], {'default': "'shadowing'", 'max_length': '255'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'seperators': ('django.db.models.fields.TextField', [], {'default': "' '"}),
            'text': ('django.db.models.fields.TextField', [], {'default': "' '"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'})
        },
        u'practices.envfocusmodel': {
            'Meta': {'object_name': 'EnvFocusModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'level': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'}),
            'words': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        u'practices.eyefocusmodel': {
            'Meta': {'object_name': 'EyeFocusModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'level': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'positions': ('django.db.models.fields.TextField', [], {'default': "'[10,10], [350,10], [10,100], [350,100]'"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'})
        },
        u'practices.gamestatsmodel': {
            'Meta': {'object_name': 'GameStatsModel'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['profiles.ProfileModel']"})
        },
        u'practices.multifocusmodel': {
            'Meta': {'object_name': 'MultiFocusModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'level': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'}),
            'words': ('django.db.models.fields.TextField', [], {'default': '"\'kelime|kelime\', \'kelime|kalima\'"'})
        },
        u'practices.tachistoscopemodel': {
            'Meta': {'object_name': 'TachistoscopeModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'level': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'}),
            'words': ('django.db.models.fields.TextField', [], {})
        },
        u'profiles.profilemodel': {
            'Meta': {'object_name': 'ProfileModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'start_date': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['auth.User']", 'unique': 'True'})
        }
    }

    complete_apps = ['practices']