# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'GameStatsModel.user'
        db.alter_column(u'practices_gamestatsmodel', 'user_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User']))

    def backwards(self, orm):

        # Changing field 'GameStatsModel.user'
        db.alter_column(u'practices_gamestatsmodel', 'user_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['profiles.ProfileModel']))

    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'practices.articlecategorymodel': {
            'Meta': {'object_name': 'ArticleCategoryModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'})
        },
        u'practices.articlemodel': {
            'Meta': {'object_name': 'ArticleModel'},
            'category': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['practices.ArticleCategoryModel']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'kind': ('django.db.models.fields.CharField', [], {'default': "'shadowing'", 'max_length': '255'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'seperators': ('django.db.models.fields.TextField', [], {'default': "' '"}),
            'text': ('django.db.models.fields.TextField', [], {'default': "' '"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'})
        },
        u'practices.envfocusmodel': {
            'Meta': {'object_name': 'EnvFocusModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'level': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'}),
            'words': ('django.db.models.fields.TextField', [], {'default': "''"})
        },
        u'practices.eyefocusmodel': {
            'Meta': {'object_name': 'EyeFocusModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('sorl.thumbnail.fields.ImageField', [], {'max_length': '100'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'level': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'positions': ('django.db.models.fields.TextField', [], {'default': "'[5,5], [90,5], [5,90], [90,90]'"}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'})
        },
        u'practices.gamestatsmodel': {
            'Meta': {'object_name': 'GameStatsModel'},
            'date': ('django.db.models.fields.DateField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'status': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'practices.multifocusmodel': {
            'Meta': {'object_name': 'MultiFocusModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'level': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'}),
            'words': ('django.db.models.fields.TextField', [], {'default': '"\'kelime|kelime\', \'kelime|kalima\'"'})
        },
        u'practices.tachistoscopemodel': {
            'Meta': {'ordering': "['level']", 'object_name': 'TachistoscopeModel'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'lang': ('django.db.models.fields.CharField', [], {'default': "'tr'", 'max_length': '255'}),
            'level': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'point': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'type': ('django.db.models.fields.CharField', [], {'default': "'y'", 'max_length': '255'}),
            'words': ('django.db.models.fields.TextField', [], {})
        }
    }

    complete_apps = ['practices']