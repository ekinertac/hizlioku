#!/usr/bin/env python
# -*- coding: utf-8 -*-

import datetime
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required

from annoying.decorators import render_to, JsonResponse
from apps.practices.models import *


def get_with_session(model, request, kind=None):
    lang = request.session['language']
    user_type = request.session['user_type']
    print user_type
    if kind:
        return model.objects.filter(lang=lang, type=user_type, kind=kind)
    else:
        return model.objects.filter(lang=lang, type=user_type)


# Applications
@login_required()
@render_to('apps/tachistoscope.html')
def tachistoscope(request, level=None):
    all_levels = get_with_session(TachistoscopeModel, request)

    if level:
        obj = get_with_session(TachistoscopeModel, request)
        obj = obj.get(level=level)

        word_list = obj.words.split(',')
        word_list = [i.strip() for i in word_list]

    else:
        url = reverse('apps.practices.views.tachistoscope', args=['1'])
        return HttpResponseRedirect(url)

    return {
        'level': level,
        'word_list': word_list,
        'all_levels': all_levels
    }


@login_required()
@render_to('apps/env_focus.html')
def env_focus(request, level=None):
    all_levels = get_with_session(EnvFocusModel, request)

    if level:
        obj = get_with_session(EnvFocusModel, request)
        obj = obj.get(level=level)

        word_list = obj.words
    else:
        url = reverse('apps.practices.views.env_focus', args=['1'])
        return HttpResponseRedirect(url)

    return {
        'level': level,
        'word_list': word_list,
        'all_levels': all_levels
    }


@login_required()
@render_to('apps/shadowing.html')
def shadowing(request, category_id=None):
    categories = get_with_session(ArticleCategoryModel, request)

    if category_id:
        articles = ArticleModel.objects.filter(category__id=category_id, kind="shadowing")

        return {
            'categories': categories,
            'articles': articles,
            'query': '?homework=%s' % request.GET.get('homework')
        }

    return {
        'categories': categories,
        'query': '?homework=%s' % request.GET.get('homework')
    }


@login_required()
@render_to('apps/shadowing_detail.html')
def shadowing_detail(request, article_id):
    article = ArticleModel.objects.get(id=article_id)

    return {
        "article": article,
        'query': '?homework=%s' % request.GET.get('homework')
    }


@login_required()
@render_to('apps/grouping.html')
def grouping(request, category_id=None):
    categories = get_with_session(ArticleCategoryModel, request)

    if category_id:
        articles = ArticleModel.objects.filter(category__id=category_id, kind="grouping")
        return {
            'categories': categories,
            'articles': articles,
            'query': '?homework=%s' % request.GET.get('homework')
        }

    return {
        'categories': categories,
        'query': '?homework=%s' % request.GET.get('homework')
    }


@login_required()
@render_to('apps/grouping_detail.html')
def grouping_detail(request, article_id):
    article = ArticleModel.objects.get(id=article_id)

    return {
        "article": article,
        'query': '?homework=%s' % request.GET.get('homework')
    }


@login_required()
@render_to('apps/multi_focus.html')
def multi_focus(request, level=None):
    all_levels = get_with_session(MultiFocusModel, request)

    if level:
        obj = get_with_session(MultiFocusModel, request)
        obj = obj.get(level=level)

        word_list = obj.words

    else:
        url = reverse('apps.practices.views.multi_focus', args=['1'])
        return HttpResponseRedirect(url)

    return {
        'level': level,
        'word_list': word_list,
        'all_levels': all_levels
    }


@login_required()
@render_to('apps/block_reading.html')
def block_reading(request, category_id=None):
    categories = get_with_session(ArticleCategoryModel, request)

    if category_id:
        articles = ArticleModel.objects.filter(category__id=category_id, kind="block_reading")
        return {
            'categories': categories,
            'articles': articles,
            'query': '?homework=%s' % request.GET.get('homework')
        }

    return {
        'categories': categories,
        'query': '?homework=%s' % request.GET.get('homework')
    }


@login_required()
@render_to('apps/block_reading_detail.html')
def block_reading_detail(request, article_id=None):
    article = ArticleModel.objects.get(id=article_id)

    return {
        "article": article,
        'query': '?homework=%s' % request.GET.get('homework')
    }


@login_required()
@render_to('apps/eye_focusing.html')
def eye_focusing(request, level=None):
    if level:

        all_levels = EyeFocusModel.objects.all()
        obj = EyeFocusModel.objects.get(level=level)

    else:
        url = reverse('eye_focusing_detail', args=['1'])
        return HttpResponseRedirect(url)

    return {
        'items': obj,
        "level": level,
        'all_levels': all_levels
    }


@login_required()
@render_to('apps/eye_muscle_development.html')
def eye_muscle_development(request):
    return {}


@login_required()
@render_to('apps/speed_reading.html')
def speed_reading(request, category_id=None):
    categories = get_with_session(ArticleCategoryModel, request)

    if category_id:
        articles = get_with_session(ArticleCategoryModel, request)
        articles = articles.get(id=category_id)

        return {
            'categories': categories,
            'articles': articles.articlemodel_set.all(),
            'query': '?homework=%s' % request.GET.get('homework')
        }

    return {
        'categories': categories,
        'query': '?homework=%s' % request.GET.get('homework')
    }


@login_required()
@render_to('apps/speed_reading_detail.html')
def speed_reading_article_detail(request, article_id):
    article = ArticleModel.objects.get(pk=article_id)
    return {
        'article': article,
        'query': '?homework=%s' % request.GET.get('homework')
    }


@login_required()
def stats(request, status, stat_type):
    if status == 'p':
        status = True
    else:
        status = False

    try:
        stat = GameStatsModel.objects.get(
            user=request.user,
            type=stat_type,
            date=datetime.date.today(),
            status=status
        )
        stat.point += 1
        stat.save()

    except GameStatsModel.DoesNotExist:
        stat = GameStatsModel(
            user=request.user,
            type=stat_type,
            date=datetime.date.today(),
            status=status,
            point=1
        )
        stat.save()

    return JsonResponse({
        'status': 'OK',
        'message': 'saved'
    })


@login_required()
@render_to('apps/performance.html')
def performance(request, event_type=None):
    from config.utils import make_epoch
    games = [{
        'name': 'Takistoskop',
        'slug': 'tachistoscope'
    }, {
        'name': 'Çevre Fokus',
        'slug': 'env_focus'
    }, {
        'name': 'Gölgeleme',
        'slug': 'shadowing'
    }, {
        'name': 'Gruplama',
        'slug': 'grouping'
    }, {
        'name': 'Multi Focus',
        'slug': 'multi_focus'
    }, {
        'name': 'Blok Okuma',
        'slug': 'block_reading'
    }, {
        'name': 'Göz Odaklama',
        'slug': 'eye_focus'
    }]

    positive = GameStatsModel.objects.filter(user=request.user, type=event_type, status=True).order_by('date')
    negative = GameStatsModel.objects.filter(user=request.user, type=event_type, status=False).order_by('date')

    print positive

    positive_data = []
    negative_data = []

    if event_type in ['tachistoscope', 'env_focus', 'multi_focus', 'eye_focus']:
        
        for pos in positive:
            positive_data.append([make_epoch(pos.date), pos.point])

        for neg in negative:
            negative_data.append([make_epoch(neg.date), neg.point])

        return JsonResponse({
            'positive': positive_data,
            'negative': negative_data
        })

    elif event_type in ['block_reading', 'shadowing', 'grouping']:
        for pos in positive:
            positive_data.append([make_epoch(pos.date), (pos.point/1000) ] )

        return JsonResponse({
            'positive': positive_data
        })
    else:
        return {
            'games': games
        }

@login_required()
def record_time(request, event_type):
    spent_time = request.GET.get('time')
    stat = GameStatsModel()
    stat.user = request.user
    stat.type = event_type
    stat.point = spent_time
    stat.date = datetime.datetime.now()
    stat.status = True
    stat.save()
    return JsonResponse('recorded')