#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Create your views here.
import datetime

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import redirect
from django.contrib.auth.models import User

from apps.profiles.models import ProfileModel

from annoying.decorators import render_to


@render_to('accounts/login.html')
def auth_login(request):

    # Check POST request
    if request.method == 'POST':
        # check fields
        username = request.POST['username']
        password = request.POST['password']

        if username and password:
            user = authenticate(username=username, password=password)

            # Check if user is exists
            if user is not None:
                # login user
                login(request, user)

                # Check users account days
                if user.profile.start_date is None:
                    # Redirect to a password change page.
                    return redirect('change_password')
                else:
                    user_start_date = user.profile.start_date
                    today = datetime.date.today()
                    date_diff = user_start_date - today

                    if abs(date_diff.days) >= 90:
                        # If user reach the 90 day limit. Deactivate account
                        user.is_active = False

                if user.is_active:
                    # set user sessions
                    request.session['language'] = user.profile.lang
                    request.session['user_type'] = user.profile.type

                    # Redirect to a success page.
                    return redirect('index')
                else:
                    # Return a 'disabled account' error message
                    return redirect('disabled_account')
            else:
                # Return an 'invalid login' error message.
                return {
                    'errors': {
                        'wrong': u'Hatalı T.C. Kimlik Numarasi yada Şifre girdiniz.'
                    }
                }
        else:
            return {
                'errors': {
                    'empty': u'Lutfen alanları doldurunuz'
                }
            }
    else:
        # Render login page if there is no POST request
        return {}


@render_to('accounts/logout.html')
def auth_logout(request):
    logout(request)
    return {}

@render_to('accounts/disabled_account.html')
def disabled_account(request):
    logout(request)
    return {}

@render_to('accounts/change_password.html')
def change_password(request):
    if request.method == 'POST':
        p1 = request.POST['password1']
        p2 = request.POST['password2']

        if p1 == p2:
            user = User.objects.get(pk=request.user.id)
            user.set_password(p1)
            
            user.save()

            profile = ProfileModel.objects.get(user__id=request.user.id)
            profile.start_date = datetime.date.today()
            profile.save()

            return redirect('/index/?status=password_changed')

        else:
            return {
                'errors': {
                    'match': 'Girdiğiniz şifreler uyuşmuyor. Lütfen tekrar deneyiniz.'
                }
            }
    return {}




















