#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from django.contrib.auth.models import User
from apps.corps.models import CorpsModel
from config.constants import LANG_CHOICES, TYPE_CHOICES, KIND_CHOICES


class ProfileModel(models.Model):
    user = models.OneToOneField(User)
    lang = models.CharField(max_length=255, choices=LANG_CHOICES, verbose_name=u'Dil',
                            help_text=u'Ders aldığı dil grubu')
    type = models.CharField(max_length=255, choices=TYPE_CHOICES, verbose_name=u'Ögrenci Tipi',
                            help_text=u'Ders aldığı alan')
    start_date = models.DateField(blank=True, null=True, verbose_name=u'Başlangıç Tarihi')
    kind = models.CharField(max_length=255, choices=KIND_CHOICES, verbose_name=u'Kullanıcı Tipi')
    corporate = models.ForeignKey(CorpsModel, verbose_name=u'Bağlı Olduğu Kurum Adı')

    class Meta:
        verbose_name = 'Profile'
        verbose_name_plural = 'Profiles'

    def get_full_name(self):
        return u"{0} {1}".format(self.first_name, self.last_name)

    def get_short_name(self):
        return self.username

    @property
    def is_teacher(self):
        return True if self.kind == 'teacher' else False

    def __unicode__(self):
        return self.user.username

User.profile = property(lambda u: u.get_profile())