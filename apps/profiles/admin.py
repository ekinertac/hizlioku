#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin as AuthUserAdmin
from django.contrib.auth.models import User
from apps.profiles.models import ProfileModel


class ProfileInline(admin.StackedInline):
    model = ProfileModel
    can_delete = False
    verbose_name_plural = 'profile'


class UserAdmin(AuthUserAdmin):
    list_display = ('username', 'email', 'first_name', 'last_name', 'is_staff')
    inlines = [ProfileInline, ]

    @staticmethod
    def first_last(obj):
        return '%s %s' % (obj.first_name, obj.last_name)






admin.site.unregister(User)
admin.site.register(User, UserAdmin)