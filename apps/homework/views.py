# Create your views here.
from django.http import Http404, HttpResponseRedirect
from django.shortcuts import redirect
from annoying.decorators import render_to, JsonResponse

from apps.homework.models import HomeWorkDayModel, HomeWorkModel, HomeWorkStatModel
from config.context import get_user_days


def homework_index(request):
    return redirect('homework_day', get_user_days(request))


@render_to('homework/index.html')
def homework_day(request, day):

    day = int(day)

    if day > get_user_days(request):
        raise Http404
    elif day < 1:
        raise Http404
    else:
        days = HomeWorkDayModel.objects.filter(day__lt=day)
        works = HomeWorkModel.objects.filter(day=day)

        response = {
            'homeworks': works,
            'days': days,
            'current_day': day
        }

        if not day == 1:
            response['prev_day'] = day - 1

        if not day == get_user_days(request):
            response['next_day'] = day + 1

        return response


def user_works(request):
    obj = HomeWorkStatModel.objects.filter(user=request.user)

    works = []
    for item in obj:
        works.append(item.homework_id)

    return JsonResponse({
        'works': works
    })


def done(request, id):
    stat = HomeWorkStatModel(
        user=request.user,
        homework_id=id
    )
    stat.save()

    return JsonResponse({
        'status': 'OK'
    })