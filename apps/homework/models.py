#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.contrib.auth.models import User
from django.db import models

from config.constants import WORK_TYPES, speed_choices, level_choices, day_choices


class HomeWorkDayModel(models.Model):
    day = models.IntegerField(verbose_name=u'Ödev Günü', choices=day_choices(), unique=True)

    class Meta:
        verbose_name = u'Ödev'
        verbose_name_plural = u'Ödevler'

    def __unicode__(self):
        return u'%s. Gün' % self.day


class HomeWorkModel(models.Model):
    day = models.ForeignKey(HomeWorkDayModel)
    title = models.CharField(max_length=255, verbose_name=u'Ödev Adı')
    work_type = models.CharField(max_length=255, choices=WORK_TYPES, verbose_name=u'Ödev Tipi')
    url = models.CharField(max_length=255, blank=True)
    is_reading = models.BooleanField(verbose_name=u'Okuma Ödevi', default=False)
    level = models.IntegerField(verbose_name=u'Ödev Seviyesi', choices=level_choices())
    speed = models.IntegerField(verbose_name=u'Ödev Hızı', choices=speed_choices())
    minute = models.IntegerField(verbose_name=u'Çalışma Süresi', help_text=u'Dakika')

    class Meta:
        verbose_name = u'Ödev'
        verbose_name_plural = u'Ödevler'
        ordering = ['id']

    def __unicode__(self):
        return u'%s. Gün - %s' % (self.day.day, self.title)

    def get_name(self):
        return u'%s' % self.title


class HomeWorkStatModel(models.Model):
    user = models.ForeignKey(User)
    homework = models.ForeignKey(HomeWorkModel)
    date = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = u'Ödev Istatistik'
        verbose_name_plural = u'Ödev Istatistikleri'

    def __unicode__(self):
        return u'%s' % self.user