#!/usr/bin/env python
# -*- coding: utf-8 -*-


from django.contrib import admin
from apps.homework.models import *


class HomeWorkModelAdmin(admin.TabularInline):
    model = HomeWorkModel


class HomeWorkDayModelAdmin(admin.ModelAdmin):
    inlines = [HomeWorkModelAdmin]

admin.site.register(HomeWorkDayModel, HomeWorkDayModelAdmin)


class HomeWorkStatAdmin(admin.ModelAdmin):
    list_display = ['user', 'homework', 'date']
    list_filter = ['user']

admin.site.register(HomeWorkStatModel, HomeWorkStatAdmin)

